FongMi影视、TVBox、猫影视配置文件。所有资源均来自于各路大神无私分享，如有遗漏，请联系删除。

一切以任何方式查看本仓库内容的人、或直接或间接使用本仓库内容的用户均应仔细阅读本声明。本仓库管理员保留随时更改或补充此免责的权利。一旦使用、复制、修改了本仓库内容，则视为您已接受此免责声明。

本仓库管理员不能保证本仓库内容的合法性、准确性、缺陷和有效性，请根据情况判断。本仓库内容，仅供测试和学习研究，禁止用于商业用途，违反规定不得将其使用国家、地区、组织等法律法规或规定的其他用途，禁止任何公众号、自媒体进行任何形式的转载、发布，请勿在境内使用本仓库内容，否则后果自负。

本仓库内容中涉及的第三方硬件、软件等，与本仓库内容没有任何直接或间接的。本仓库内容仅对部署和使用过程进行调查描述，不代表支持使用任何第三方硬件、软件。使用任何第三方硬件、软件，所造成的后果都是由使用的个人或组织承担的，与本仓库内容相关。

所有直接或间接使用本仓库内容的个人和组织，应24小时内完成学习和研究，并及时删除本仓库内容。如对本仓库内容的功能有需求，应自行开发相关功能。所有基于本仓库内容的来源代码，进行的任何，对于其他个人或组织的自发行为，与本仓库内容没有任何直接或关联的关系，所造成的一切修改后果亦与本仓库内容和本仓库管理员无关。

DNS配置为公共dns不直接用运营商dns否则建议有些域名建议被污染。例如：cdnrrs.gz.chinamobile.com联通电信自家dns解析就无法打开。可用阿里腾讯百度114等公共dns tvbox可用在设置里面选择外部dns

（建议下载使用作为本地源，因为会出现违规内容导致不能使用）

电视盒配置：

（1）0707.json峰米影视多线配置接口，仅适用于峰米影视；

（2）0821.json庞大而齐全的配置，在太硬配置的基础上添加了一些优质的播源、直播线路和解析；

（3）0822.json极简配置，可以大佬的jar，还包括几条路飞、俊于的源。

（4）0825.json小而精的配置，jar包来源于Panda Groove的go包，其中泥巴、星星等，需要替换成自己的代理url；

（5）0826.json来源于饭太硬的jar包和配置；

（6）0827.json jar包和配置来源于fongmi；

（7）0828.json jar包和配置来源于唐三；

（8）js.json jar包来源于Panda Groove的go包，资源来源于道长drpy(js)仓库添加YouTube直播；

（9）XBPQ.json XBPQ源，jar包和配置来源于小米小爆脾气；

（10）XYQ.json XYQ源，jar包和配置来源于香雅情；

（11）cat.json cat源，资源来源于网络各路大佬。/cat/js 暴猫影视可直接食用；

（12） jsm.json 来自js.json 修改家庭电视可用主要删除YouTube直播。

（13）电视安卓版本低于4.4以下下载apk：https://github.com/o0HalfLife0o/TVBoxOSC/releases/download/20230902-0114/TVBox_q215613905_20230902-0114.apk

猫影视使用github配置

配置教程：https://omii.top/1296.html

APP推荐：
（1）FongMi版本项目地址：https://github.com/FongMi/TV

（2）q215613905版本 项目地址：https://github.com/q215613905/TVBoxOS

（3）takagen99版本项目地址：https://github.com/takagen99/Box

（4）皮虾皮版本发布频道：https://t.me/pipixiawerun

（5）新版猫影视项目地址：https://github.com/catvod/CatVodOpen


TVBox各路大佬配置（排名不分前台）：


（1）俊于：http://home.jundie.top:81/top98.json

（2）饭太硬：http://饭太硬.top/tv

（3）霜辉月明py：https://mirror.ghproxy.com/raw.githubusercontent.com/lm317379829/PyramidStore/pyramid/py.json

（4）菜妮丝：https://tvbox.cainisi.cf

（5）南风：https://agit.ai/Yoursmile7/TVBox/raw/branch/master/XC.json

（6）巧技：http://pandown.pro/tvbox/tvbox.json

（7）那里花开：http://hz752.love:63/tk.json

（8）胖虎：https://notabug.org/imbig66/tv-spider-man/raw/master/配置/0801.json

（9）Yoursmile7：https://agit.ai/Yoursmile7/TVBox/raw/branch/master/XC.json

（10）雷：https://mirror.ghproxy.com/https://raw.githubusercontent.com/dxawi/0/main/0.json

（11）电视（自用）：https://github.moeyy.xyz/raw.githubusercontent.com/qist/tvbox/master/jsm.json

随机轮换列表：
（1）https://bing.img.run/rand.php

（2）http://www.kf666888.cn/api/tvbox/img

（3）https://picsum.photos/1280/720/?blur=10

（4）http://刚刚.live/图

（5）http://饭.eu.org/分析/api.php ,

（6）https://www.dmoe.cc/random.php

（7）https://api.btstu.cn/sjbz/zsy.php

（8）https://api.btstu.cn/sjbz/?lx=dongman

（9）http://api.btstu.cn/sjbz/?lx=meizi

（10）http://api.btstu.cn/sjbz/?lx=suiji

（11）https://pictures.catvod.eu.org/

自用仓库，如果喜欢，请Fork自用，谢谢！

尽自己所能更新，不保证配置的有效性和时效性。